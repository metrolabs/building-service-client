/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

import io.metro.buildings.BuildingClient;
import io.metro.buildings.models.buildings.Building;
import io.metro.buildings.models.buildings.Detail;
import io.metro.buildings.models.buildings.Use;
import io.metro.measurables.models.Measurable;
import io.metro.opportunities.models.RetrofitOpportunity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestConfig.class)
@SpringBootTest(classes= BuildingClient.class)
public class BuildingClientTest {

    @Autowired
    private BuildingClient client;

    private String buildingId = "BUILDING1471994530429QNEjxZ";
    private String buildingUseId = "BUILDINGUSE1474380812869N3mkfz";
    private String buildingUseDetailId = "BUILDINGUSEDETAIL14748232398461LmCyg";

    @Before
    public void before() {
        client.init();
    }

    @Test
    public void getBuilding() {
        ResponseEntity<Resource<Building>> response = client.getBuilding(buildingId);
        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertTrue(response.getBody().getId().getHref().endsWith(buildingId));
    }

    @Test
    public void findBuildings() {
        ResponseEntity<PagedResources<Resource<Building>>> response = client.findBuildings(0, 10);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());

        assertThat(response.getBody().getMetadata()).hasFieldOrProperty("totalPages");
        assertThat(response.getBody().getMetadata()).hasFieldOrProperty("totalElements");
        assertThat(response.getBody()).hasAtLeastOneElementOfType(Resource.class);
    }

    @Test
    public void getBuildingOpportunitiesCollection() {
        ResponseEntity<Resources<Resource<RetrofitOpportunity>>> response =
                client.getBuildingOpportunitiesCollection(buildingId);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertThat(response.getBody().getContent()).hasAtLeastOneElementOfType(Resource.class);
    }

    @Test
    public void getBuildingMeasurablesCollection() {
        ResponseEntity<Resources<Resource<Measurable>>> response =
                client.getBuildingMeasurablesCollection(buildingId);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertThat(response.getBody().getContent()).hasAtLeastOneElementOfType(Resource.class);
    }

    @Test
    public void getBuildingUseCollection() {
        ResponseEntity<Resources<Resource<Use>>> response =
                client.getBuildingUseCollection(buildingId);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertThat(response.getBody().getContent()).hasAtLeastOneElementOfType(Resource.class);
    }

    @Test
    public void getUse() {
        ResponseEntity<Resource<Use>> response = client.getUse(buildingUseId);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
    }

    @Test
    public void getBuildingUseDetailCollection() {
        ResponseEntity<Resources<Resource<Detail>>> response = client.getBuildingUseDetailCollection(buildingUseId);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertThat(response.getBody().getContent()).hasAtLeastOneElementOfType(Resource.class);
    }

    @Test
    public void getUseDetail() {
        ResponseEntity<Resource<Detail>> response = client.getUseDetail(buildingUseDetailId);

        assertTrue(response.hasBody());
        assertTrue(response.getStatusCode().is2xxSuccessful());
    }
}
