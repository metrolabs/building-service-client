/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings.models.buildings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.HashSet;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Use {
    private String id;
    private String accountId;
    private String buildingId;
    private String useName;
    private Set<Detail> useDetails = new HashSet<>();

    public String getId() {
        return id;
    }

    public Use setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public Use setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public Use setBuildingId(String buildingId) {
        this.buildingId = buildingId;
        return this;
    }

    public String getUseName() {
        return useName;
    }

    public Set<Detail> getUseDetails() {
        return useDetails;
    }

    public Use setUseDetails(Set<Detail> useDetails) {
        this.useDetails = useDetails;
        return this;
    }

    public Use setUseName(String uaseName) {
        this.useName = uaseName;
        return this;
    }
}
