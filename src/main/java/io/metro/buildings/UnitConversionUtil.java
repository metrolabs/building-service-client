/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings;

import io.metro.buildings.models.buildings.Building;

public class UnitConversionUtil {
    public static Integer toSquareMeter(Building building) {
        int o = building.getGrossFloorArea();
        if (building.getGrossFloorAreaUnit().equals(Building.GrossFloorAreaUnitsType.SQUARE_FEET)) {
            o = (int)(building.getGrossFloorArea()*0.092903);
        }

        return o;
    }

    public static Integer toSquareFeet(Building building) {
        int o = building.getGrossFloorArea();
        if (building.getGrossFloorAreaUnit().equals(Building.GrossFloorAreaUnitsType.SQUARE_METERS)) {
            o = (int)(building.getGrossFloorArea()*10.7639);
        }

        return o;
    }
}
