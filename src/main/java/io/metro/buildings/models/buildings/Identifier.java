/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings.models.buildings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Identifier {
    private Long id;
    private String accountId;
    private String buildingId;
    private String identifierType;
    private String identifier;

    public Long getId() {
        return id;
    }

    public Identifier setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public Identifier setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public Identifier setBuildingId(String buildingId) {
        this.buildingId = buildingId;
        return this;
    }

    public String getIdentifierType() {
        return identifierType;
    }

    public Identifier setIdentifierType(String identifierType) {
        this.identifierType = identifierType;
        return this;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Identifier setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }
}
