/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.util.Assert;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * Custom DTO to implement binding response representations of pageable collections.
 *
 * @author William Gayton
 */
@XmlRootElement(name = "pagedEntities")
public class MetroPagedResources<T> extends Resources<T> {

    public static MetroPagedResources<?> NO_PAGE = new MetroPagedResources<>();

    private MetroPagedResources.PageMetadata metadata;

    private Map<String, Object> metaProperties = new HashMap<>();

    /**
     * Default constructor to allow instantiation by reflection.
     */
    protected MetroPagedResources() {
        this(new ArrayList<T>(), null);
    }

    /**
     * Creates a new {@link PagedResources} from the given content, {@link PagedResources.PageMetadata} and {@link Link}s (optional).
     *
     * @param content must not be {@literal null}.
     * @param metadata
     * @param links
     */
    public MetroPagedResources(Collection<T> content, MetroPagedResources.PageMetadata metadata, Link... links) {
        this(content, metadata, Arrays.asList(links));
    }

    /**
     * Creates a new {@link PagedResources} from the given content {@link PagedResources.PageMetadata} and {@link Link}s.
     *
     * @param pagedResources must not be {@literal null}.
     */
    public MetroPagedResources(PagedResources pagedResources) {
        super(pagedResources.getContent(), pagedResources.getLinks());
        long size = pagedResources.getMetadata().getSize();
        long number = pagedResources.getMetadata().getNumber();
        long totalElements = pagedResources.getMetadata().getTotalElements();
        long totalPages = pagedResources.getMetadata().getTotalPages();
        this.metadata = new PageMetadata(size, number, totalElements, totalPages);
    }

    /**
     * Creates a new {@link PagedResources} from the given content {@link PagedResources.PageMetadata} and {@link Link}s.
     *
     * @param content must not be {@literal null}.
     * @param metadata
     * @param links
     */
    public MetroPagedResources(Collection<T> content, PagedResources.PageMetadata metadata, Iterable<Link> links) {
        super(content, links);

        long size = metadata.getSize();
        long number = metadata.getNumber();
        long totalElements = metadata.getTotalElements();
        long totalPages = metadata.getTotalPages();
        this.metadata = new PageMetadata(size, number, totalElements, totalPages);
    }

    /**
     * Creates a new {@link PagedResources} from the given content {@link PagedResources.PageMetadata} and {@link Link}s.
     *
     * @param content must not be {@literal null}.
     * @param metadata
     * @param links
     */
    public MetroPagedResources(Collection<T> content, MetroPagedResources.PageMetadata metadata, Iterable<Link> links) {
        super(content, links);
        this.metadata = metadata;
    }

    /**
     * Returns the pagination metadata.
     *
     * @return the metadata
     */
    @JsonProperty("page")
    public MetroPagedResources.PageMetadata getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public Map<String, Object> getMetaProperties() {
        return metaProperties;
    }

    /**
     * Factory method to easily create a {@link PagedResources} instance from a set of entities and pagination metadata.
     *
     * @param content must not be {@literal null}.
     * @param metadata
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T extends Resource<S>, S> MetroPagedResources<T> wrap(Iterable<S> content, MetroPagedResources.PageMetadata metadata) {

        Assert.notNull(content);
        ArrayList<T> resources = new ArrayList<T>();

        for (S element : content) {
            resources.add((T) new Resource<S>(element));
        }

        return new MetroPagedResources<T>(resources, metadata);
    }

    /**
     * Returns the Link pointing to the next page (if set).
     *
     * #addPaginationLinks(Link)
     * @return
     */
    @JsonIgnore
    public Link getNextLink() {
        return getLink(Link.REL_NEXT);
    }

    /**
     * Returns the Link pointing to the previous page (if set).
     *
     * #addPaginationLinks(Link)
     * @return
     */
    @JsonIgnore
    public Link getPreviousLink() {
        return getLink(Link.REL_PREVIOUS);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.hateoas.ResourceSupport#toString()
     */
    @Override
    public String toString() {
        return String.format("PagedResource { content: %s, metadata: %s, links: %s }", getContent(), metadata, getLinks());
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.hateoas.Resources#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || !getClass().equals(obj.getClass())) {
            return false;
        }

        MetroPagedResources<?> that = (MetroPagedResources<?>) obj;
        boolean metadataEquals = this.metadata == null ? that.metadata == null : this.metadata.equals(that.metadata);

        return metadataEquals ? super.equals(obj) : false;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.hateoas.Resources#hashCode()
     */
    @Override
    public int hashCode() {

        int result = super.hashCode();
        result += this.metadata == null ? 0 : 31 * this.metadata.hashCode();
        return result;
    }

    /**
     * Value object for pagination metadata.
     *
     * @author Oliver Gierke
     */
    public static class PageMetadata {

        @XmlAttribute
        @JsonProperty private long size;
        @XmlAttribute @JsonProperty private long totalElements;
        @XmlAttribute @JsonProperty private long totalPages;
        @XmlAttribute @JsonProperty private long number;

        protected PageMetadata() {
        }

        /**
         * Creates a new {@link MetroPagedResources.PageMetadata} from the given size, number, total elements and total pages.
         *
         * @param size
         * @param number zero-indexed, must be less than totalPages
         * @param totalElements
         * @param totalPages
         */
        public PageMetadata(long size, long number, long totalElements, long totalPages) {

            Assert.isTrue(size > -1, "Size must not be negative!");
            Assert.isTrue(number > -1, "Number must not be negative!");
            Assert.isTrue(totalElements > -1, "Total elements must not be negative!");
            Assert.isTrue(totalPages > -1, "Total pages must not be negative!");

            this.size = size;
            this.number = number;
            this.totalElements = totalElements;
            this.totalPages = totalPages;
        }

        /**
         * Creates a new {@link MetroPagedResources.PageMetadata} from the given size, numer and total elements.
         *
         * @param size the size of the page
         * @param number the number of the page
         * @param totalElements the total number of elements available
         */
        public PageMetadata(long size, long number, long totalElements) {
            this(size, number, totalElements, size == 0 ? 0 : (long) Math.ceil((double) totalElements / (double) size));
        }

        /**
         * Returns the requested size of the page.
         *
         * @return the size a positive long.
         */
        public long getSize() {
            return size;
        }

        /**
         * Returns the total number of elements available.
         *
         * @return the totalElements a positive long.
         */
        public long getTotalElements() {
            return totalElements;
        }

        /**
         * Returns how many pages are available in total.
         *
         * @return the totalPages a positive long.
         */
        public long getTotalPages() {
            return totalPages;
        }

        /**
         * Returns the number of the current page.
         *
         * @return the number a positive long.
         */
        public long getNumber() {
            return number;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return String.format("Metadata { number: %d, total pages: %d, total elements: %d, size: %d }",
                    number, totalPages, totalElements, size);
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {

            if (this == obj) {
                return true;
            }

            if (obj == null || !obj.getClass().equals(getClass())) {
                return false;
            }

            MetroPagedResources.PageMetadata that = (MetroPagedResources.PageMetadata) obj;

            return this.number == that.number && this.size == that.size && this.totalElements == that.totalElements
                    && this.totalPages == that.totalPages;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {

            int result = 17;
            result += 31 * (int) (this.number ^ this.number >>> 32);
            result += 31 * (int) (this.size ^ this.size >>> 32);
            result += 31 * (int) (this.totalElements ^ this.totalElements >>> 32);
            result += 31 * (int) (this.totalPages ^ this.totalPages >>> 32);
            return result;
        }
    }
}
