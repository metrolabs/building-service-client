/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated  
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings.models.location;

public class Period {
    /**
     * When the Place opens.
     */
    public OpenClose open;

    /**
     * When the Place closes.
     */
    public OpenClose close;

    public class OpenClose {
        /**
         * Day that this Open/Close pair is for.
         */
        public LocationDetails.DayOfWeek day;

        /**
         * Time that this Open or Close happens at.
         */
        public Object time;
    }
}
