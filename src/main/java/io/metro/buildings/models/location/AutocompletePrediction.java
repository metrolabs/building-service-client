/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings.models.location;

public class AutocompletePrediction {

    /**
     * Description of the matched prediction.
     */
    public String description;

    /**
     * The Place ID of the place.
     */
    public String placeId;

    /**
     * types is an array indicating the type of the address component.
     *
     * <p>Please see <a href="https://developers.google.com/places/supported_types">supported
     * types</a> for a list of types that can be returned.</p>
     */
    public String types[];

    /**
     * terms contains an array of terms identifying each section of the returned description (a
     * section of the description is generally terminated with a comma). Each entry in the array has a
     * value field, containing the text of the term, and an offset field, defining the start position
     * of this term in the description, measured in Unicode characters.
     */
    public Term terms[];

    /**
     * MatchedSubstring describes the location of the entered term in the prediction result text, so
     * that the term can be highlighted if desired.
     */
    public static class MatchedSubstring {

        /**
         * length describes the length of the matched substring.
         */
        public int length;

        /**
         * offset defines the start position of the matched substring.
         */
        public int offset;
    }

    public MatchedSubstring matchedSubstrings[];

    /**
     * Term identifies each section of the returned description (a section of the description is
     * generally terminated with a comma).
     */
    public static class Term {

        /**
         * offset defines the start position of this term in the description, measured in Unicode
         * characters.
         */
        public int offset;

        /**
         * The text of the matched term.
         */
        public String value;
    }
}
