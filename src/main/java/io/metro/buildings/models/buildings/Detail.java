/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings.models.buildings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Detail {
    private String id;
    private String accountId;
    private String buildingUseId;
    private String detailName;
    private String units;
    private String value;

    public String getId() {
        return id;
    }

    public Detail setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public Detail setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getBuildingUseId() {
        return buildingUseId;
    }

    public Detail setBuildingUseId(String buildingUseId) {
        this.buildingUseId = buildingUseId;
        return this;
    }

    public String getDetailName() {
        return detailName;
    }

    public Detail setDetailName(String detailName) {
        this.detailName = detailName;
        return this;
    }

    public String getUnits() {
        return units;
    }

    public Detail setUnits(String units) {
        this.units = units;
        return this;
    }

    public String getValue() {
        return value;
    }

    public Detail setValue(String value) {
        this.value = value;
        return this;
    }
}
