/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings;

import io.metro.buildings.models.buildings.Building;
import io.metro.buildings.models.buildings.Detail;
import io.metro.buildings.models.buildings.Identifier;
import io.metro.buildings.models.buildings.Use;
import io.metro.buildings.models.location.AutocompletePrediction;
import io.metro.buildings.models.location.LocationAddress;
import io.metro.measurables.models.Measurable;
import io.metro.opportunities.models.RetrofitOpportunity;
import io.metro.specification.Filter;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.geo.Metrics;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.Charset;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertValueNotNull;

@Service
public class BuildingClient {
    private final Logger logger = LoggerFactory.getLogger(BuildingClient.class);

    @Value("${spring.api.gateway-service}")
    private String SERVICE;
    @Value("${spring.api.username}")
    private String username;
    @Value("${spring.api.password}")
    private String password;

    private final int DEFAULT_PAGINATION_PAGE = 0;
    private final int DEFAULT_PAGINATION_SIZE = 10;
    private final int DEFAULT_GEO_DISTANCE = 50;
    private final Metrics DEFAULT_GEO_METRIC = Metrics.KILOMETERS;

    private static final String USES = "/uses";
    private static final String DETAILS = "/details";
    private static final String BUILDINGS = "/buildings";
    private static final String IDENTIFIERS = "/identifiers";
    private static final String OPPORTUNITIES = "/opportunities";
    private static final String MEASURABLES = "/measurables";
    private static final String LOCATIONS = "/locations";
    private static final String SEARCH = "/search";
    private static final String LOCATE = "/locate";

    private HttpHeaders headers;
    private RestTemplate restTemplate;

    @Autowired
    public BuildingClient(@LoadBalanced RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.headers = new HttpHeaders();
    }

    public BuildingClient init() {
        System.out.println("BuildingsClient PostConstruct Initialized");
        logger.debug("");
        logger.debug("====================================================================================");
        logger.debug("=========================== initializing BuildingsClient ===========================");
        logger.debug("Authenticating Client with username: {} and password: {}", username, password);
        authenticate(username, password);
        logger.debug("Setting header X-Forwarded-Host: {}", getXForwardedHost(SERVICE));
        headers.set("X-Forwarded-Host", getXForwardedHost(SERVICE));
        logger.debug("===========================   initialization completed   ===========================");
        logger.debug("");

        return this;
    }

    private String getXForwardedHost(String url) {
        if(url.toLowerCase().contains("https://")) {
            return url.toLowerCase().replaceFirst("https://", "");
        }
        else if (url.toLowerCase().contains("http://")) {
            return url.toLowerCase().replaceFirst("http://", "");
        }

        return url;
    }

    public BuildingClient authenticate(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String( encodedAuth );
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, authHeader);
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        HttpEntity<Object> response = restTemplate.exchange(SERVICE, HttpMethod.GET, httpEntity, Object.class);

        String sessionToken = response.getHeaders().getFirst("x-auth-token");
        assertValueNotNull(sessionToken, "Response: x-auth-token");

        withAuthToken(sessionToken);
        logger.debug("Setting header X-Auth-Token: {}", sessionToken);

        return this;
    }

    /**
     * Override the URL of the service to use.
     * Note: URL is stripped of trailing slash.
     *
     * @param url URL of service
     * @return this client
     */
    public BuildingClient withUrl(String url) {
        SERVICE = url.substring(0, url.length() - (url.endsWith("/") ? 1 : 0));
        return this;
    }
    

    public BuildingClient withAuthToken(String auth) {
        headers.set("x-auth-token", auth);
        return this;
    }
    
    public ResponseEntity<Resource<Building>> getBuilding(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();
        return getBuilding(new Link(builder.toString()));
    }

    public ResponseEntity<Resource<Building>> getBuilding(Link link) {
        return restTemplate.exchange(link.getHref(), HttpMethod.GET, new HttpEntity<>(headers), 
                new ParameterizedTypeReference<Resource<Building>>() {});
    }

    public ResponseEntity<PagedResources<Resource<Building>>> findBuildings(int page, int size) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .queryParam("page", page)
                .queryParam("limit", size)
                .build();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers), 
                new ParameterizedTypeReference<PagedResources<Resource<Building>>>() {});
    }

    public ResponseEntity<Resource<Building>> createBuilding(Building entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .build(true);

        return restTemplate.exchange(builder.toUri(), HttpMethod.POST, new HttpEntity<>(entity, headers), 
                new ParameterizedTypeReference<Resource<Building>>() {});
    }

    public ResponseEntity<Resource<Building>> updateBuilding(String id, Building entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return updateBuilding(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Building>> updateBuilding(Link link, Building entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Building>> PTR = new ParameterizedTypeReference<Resource<Building>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.PUT, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Building>> patchBuilding(String id, Building entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return patchBuilding(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Building>> patchBuilding(Link link, Building entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Building>> PTR =
                new ParameterizedTypeReference<Resource<Building>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.PATCH, REQUEST, PTR);
    }

    public ResponseEntity deleteBuilding(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return deleteBuilding(new Link(builder.toString()));
    }

    public ResponseEntity deleteBuilding(Link link) {
        HttpEntity<?> REQUEST = new HttpEntity<>(headers);

        return restTemplate.exchange(link.getHref(), HttpMethod.DELETE, REQUEST, Object.class);
    }


    public ResponseEntity<Resources<Resource<RetrofitOpportunity>>> getBuildingOpportunitiesCollection(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(OPPORTUNITIES)
                .buildAndExpand(id)
                .encode();

        return getBuildingOpportunitiesCollection(new Link(builder.toString()));
    }

    public ResponseEntity<Resources<Resource<RetrofitOpportunity>>> getBuildingOpportunitiesCollection(Link link) {
        return restTemplate.exchange(link.getHref(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resources<Resource<RetrofitOpportunity>>> () {});
    }

    public ResponseEntity<Resource<RetrofitOpportunity>> createOpportunities(String id, RetrofitOpportunity entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(OPPORTUNITIES)
                .buildAndExpand(id)
                .encode();

        return createOpportunities(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<RetrofitOpportunity>> createOpportunities(Link link, RetrofitOpportunity entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<RetrofitOpportunity>> PTR =
                new ParameterizedTypeReference<Resource<RetrofitOpportunity>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.POST, REQUEST, PTR);
    }


    public ResponseEntity<Resources<Resource<Measurable>>> getBuildingMeasurablesCollection(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(MEASURABLES)
                .buildAndExpand(id)
                .encode();

        return getBuildingMeasurablesCollection(new Link(builder.toString()));
    }

    public ResponseEntity<Resources<Resource<Measurable>>> getBuildingMeasurablesCollection(Link link) {
        return restTemplate.exchange(link.getHref(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resources<Resource<Measurable>>> () {});
    }

    public ResponseEntity<Resource<Measurable>> createMeasurables(String id, Measurable entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(MEASURABLES)
                .buildAndExpand(id)
                .encode();

        return createMeasurables(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Measurable>> createMeasurables(Link link, Measurable entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Measurable>> PTR =
                new ParameterizedTypeReference<Resource<Measurable>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.POST, REQUEST, PTR);
    }
    


    public ResponseEntity<Resources<Resource<Use>>> getBuildingUseCollection(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(USES)
                .buildAndExpand(id)
                .encode();

        return getBuildingUseCollection(new Link(builder.toString()));
    }

    public ResponseEntity<Resources<Resource<Use>>> getBuildingUseCollection(Link link) {
        return restTemplate.exchange(link.getHref(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resources<Resource<Use>>> () {});
    }

    public ResponseEntity<Resource<Use>> createUse(String id, Use entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(USES)
                .buildAndExpand(id)
                .encode();

        return createUse(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Use>> createUse(Link link, Use entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Use>> PTR =
                new ParameterizedTypeReference<Resource<Use>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.POST, REQUEST, PTR);
    }
    
    public ResponseEntity<Resource<Use>> getUse(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(USES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return getUse(new Link(builder.toString()));
    }

    public ResponseEntity<Resource<Use>> getUse(Link link) {
        HttpEntity<?> REQUEST = new HttpEntity<>(headers);
        ParameterizedTypeReference<Resource<Use>> PTR =
                new ParameterizedTypeReference<Resource<Use>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.GET, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Use>> updateUse(String id, Use entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(USES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return updateUse(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Use>> updateUse(Link link, Use entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Use>> PTR =
                new ParameterizedTypeReference<Resource<Use>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.PUT, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Use>> patchUse(String id, Use entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(USES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return patchUse(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Use>> patchUse(Link link, Use entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Use>> PTR =
                new ParameterizedTypeReference<Resource<Use>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.PATCH, REQUEST, PTR);
    }

    public ResponseEntity deleteUse(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(USES)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return deleteUse(new Link(builder.toString()));
    }

    public ResponseEntity deleteUse(Link link) {
        HttpEntity<?> REQUEST = new HttpEntity<>(headers);

        return restTemplate.exchange(link.getHref(), HttpMethod.DELETE, REQUEST, Object.class);
    }



    public ResponseEntity<Resources<Resource<Detail>>> getBuildingUseDetailCollection(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(USES)
                .path("/{id}")
                .path(DETAILS)
                .buildAndExpand(id)
                .encode();

        return getBuildingUseDetailCollection(new Link(builder.toString()));
    }

    public ResponseEntity<Resources<Resource<Detail>>> getBuildingUseDetailCollection(Link link) {
        return restTemplate.exchange(link.getHref(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resources<Resource<Detail>>> () {});
    }

    public ResponseEntity<Resource<Detail>> createUseDetail(String id, Detail entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(USES)
                .path("/{id}")
                .path(DETAILS)
                .buildAndExpand(id)
                .encode();

        return createUseDetail(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Detail>> createUseDetail(Link link, Detail entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Detail>> PTR =
                new ParameterizedTypeReference<Resource<Detail>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.POST, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Detail>> getUseDetail(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(DETAILS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return getUseDetail(new Link(builder.toString()));
    }

    public ResponseEntity<Resource<Detail>> getUseDetail(Link link) {
        HttpEntity<?> REQUEST = new HttpEntity<>(headers);
        ParameterizedTypeReference<Resource<Detail>> PTR =
                new ParameterizedTypeReference<Resource<Detail>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.GET, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Detail>> updateUseDetail(String id, Detail entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(DETAILS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return updateUseDetail(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Detail>> updateUseDetail(Link link, Detail entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Detail>> PTR =
                new ParameterizedTypeReference<Resource<Detail>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.PUT, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Detail>> patchUseDetail(String id, Detail entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(DETAILS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return patchUseDetail(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Detail>> patchUseDetail(Link link, Detail entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Detail>> PTR =
                new ParameterizedTypeReference<Resource<Detail>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.PATCH, REQUEST, PTR);
    }

    public ResponseEntity deleteUseDetail(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(DETAILS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return deleteUseDetail(new Link(builder.toString()));
    }

    public ResponseEntity deleteUseDetail(Link link) {
        HttpEntity<?> REQUEST = new HttpEntity<>(headers);

        return restTemplate.exchange(link.getHref(), HttpMethod.DELETE, REQUEST, Object.class);
    }



    public ResponseEntity<Resources<Resource<Identifier>>> getBuildingIdentifierCollection(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(IDENTIFIERS)
                .buildAndExpand(id)
                .encode();

        return getBuildingIdentifierCollection(new Link(builder.toString()));
    }

    public ResponseEntity<Resources<Resource<Identifier>>> getBuildingIdentifierCollection(Link link) {
        return restTemplate.exchange(link.getHref(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<Resources<Resource<Identifier>>> () {});
    }

    public ResponseEntity<Resource<Identifier>> getIdentifier(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(IDENTIFIERS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return getIdentifier(new Link(builder.toString()));
    }

    public ResponseEntity<Resource<Identifier>> getIdentifier(Link link) {
        HttpEntity<?> REQUEST = new HttpEntity<>(headers);
        ParameterizedTypeReference<Resource<Identifier>> PTR =
                new ParameterizedTypeReference<Resource<Identifier>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.GET, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Identifier>> createIdentifier(String id, Identifier entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/{id}")
                .path(IDENTIFIERS)
                .buildAndExpand(id)
                .encode();

        return createIdentifier(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Identifier>> createIdentifier(Link link, Identifier entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Identifier>> PTR =
                new ParameterizedTypeReference<Resource<Identifier>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.POST, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Identifier>> updateIdentifier(String id, Identifier entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(IDENTIFIERS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return updateIdentifier(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Identifier>> updateIdentifier(Link link, Identifier entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Identifier>> PTR =
                new ParameterizedTypeReference<Resource<Identifier>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.PUT, REQUEST, PTR);
    }

    public ResponseEntity<Resource<Identifier>> patchIdentifier(String id, Identifier entity) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(IDENTIFIERS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return patchIdentifier(new Link(builder.toString()), entity);
    }

    public ResponseEntity<Resource<Identifier>> patchIdentifier(Link link, Identifier entity) {
        HttpEntity<?> REQUEST = new HttpEntity<>(entity, headers);
        ParameterizedTypeReference<Resource<Identifier>> PTR =
                new ParameterizedTypeReference<Resource<Identifier>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.PATCH, REQUEST, PTR);
    }

    public ResponseEntity deleteIdentifier(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(IDENTIFIERS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return deleteIdentifier(new Link(builder.toString()));
    }

    public ResponseEntity deleteIdentifier(Link link) {
        HttpEntity<?> REQUEST = new HttpEntity<>(headers);

        return restTemplate.exchange(link.getHref(), HttpMethod.DELETE, REQUEST, Object.class);
    }


    public ResponseEntity<MetroPagedResources<Resource<Building>>>
    searchForBuildings(Filter filter, int page, int size) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/search")
                .queryParam("page", page)
                .queryParam("limit", size)
                .build();

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, new HttpEntity<>(headers),
                new ParameterizedTypeReference<MetroPagedResources<Resource<Building>>>() {});
    }

    public ResponseEntity<List<String>> searchForBuildingIdCollection(Filter filter) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(BUILDINGS)
                .path("/search")
                .path("/building-id-collection");

        HttpEntity<?> REQUEST = new HttpEntity<>(filter, headers);
        ParameterizedTypeReference<List<String>> PTR =
                new ParameterizedTypeReference<List<String>>() {};
        return restTemplate.exchange(builder.build().toUri(), HttpMethod.POST, REQUEST, PTR);
    }


    public Resources<AutocompletePrediction> searchAddress(String address) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(LOCATIONS)
                .path("/search")
                .queryParam("address", address)
                .build();

        HttpEntity<?> REQUEST = new HttpEntity<>(headers);
        ParameterizedTypeReference<Resources<AutocompletePrediction>> PTR =
                new ParameterizedTypeReference<Resources<AutocompletePrediction>>() {};

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, REQUEST, PTR).getBody();
    }

    public ResponseEntity<Resources<Resource<AutocompletePrediction>>>
    locateAddress(String route, String locality, String admin, String postal, String country) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(LOCATIONS)
                .path(LOCATE)
                .queryParam("route", route)
                .queryParam("locality", locality)
                .queryParam("administrative_area", admin)
                .queryParam("postal_code", postal)
                .queryParam("country", country)
                .build(true);

        HttpEntity<?> REQUEST = new HttpEntity<>(headers);
        ParameterizedTypeReference<Resources<Resource<AutocompletePrediction>>> PTR =
                new ParameterizedTypeReference<Resources<Resource<AutocompletePrediction>>>() {};

        return restTemplate.exchange(builder.toUri(), HttpMethod.GET, REQUEST, PTR);
    }

    public ResponseEntity<Resource<LocationAddress>> getPlaceDetails(String id) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(SERVICE)
                .path(LOCATIONS)
                .path("/{id}")
                .buildAndExpand(id)
                .encode();

        return getPlaceDetails(new Link(builder.toString()));
    }

    public ResponseEntity<Resource<LocationAddress>> getPlaceDetails(Link link) {
        HttpEntity<?> REQUEST = new HttpEntity<>(headers);
        ParameterizedTypeReference<Resource<LocationAddress>> PTR =
                new ParameterizedTypeReference<Resource<LocationAddress>>() {};

        return restTemplate.exchange(link.getHref(), HttpMethod.GET, REQUEST, PTR);
    }
}
