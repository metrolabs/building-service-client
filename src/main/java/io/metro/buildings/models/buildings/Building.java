/*
 * ________________________________________________________________________
 * METRO.IO CONFIDENTIAL
 * ________________________________________________________________________
 *
 * Copyright (c) 2017.
 * Metro Labs Incorporated
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Metro Labs Incorporated and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Metro Labs Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Metro Labs Incorporated.
 */

package io.metro.buildings.models.buildings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.metro.buildings.models.location.LocationAddress;
import io.metro.measurables.models.ClimateZones;

import java.util.Date;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Building {
    private String id;
    private String accountId;
    private String name;
    private String address;
    private String address2;
    private String city;
    private String country;
    private String stateProvence;
    private String postal;
    private Double latitude;
    private Double longitude;
    private ClimateZones koppen;
    private String noaaStationId;
    private String epwDataId;
    private Integer yearBuilt;
    private PrimaryBuildingType buildingType;
    private Integer grossFloorAreaM2;
    private Integer grossFloorAreaFt2;
    private Integer grossFloorArea;
    private GrossFloorAreaUnitsType grossFloorAreaUnit;
    private Double energyConsumptionIntensity;
    private Double energyCostIntensity;
    private Double emissionsIntensity;
    private Date lastMeasurementDate;
    private String notes;

    private Set<Use> uses;
    private Set<Identifier> identifiers;
    private LocationAddress locationDetails;

    public String getId() {
        return id;
    }

    public Building setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public Building setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Building setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Building setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getAddress2() {
        return address2;
    }

    public Building setAddress2(String address2) {
        this.address2 = address2;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Building setCity(String city) {
        this.city = city;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Building setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getStateProvence() {
        return stateProvence;
    }

    public Building setStateProvence(String stateProvence) {
        this.stateProvence = stateProvence;
        return this;
    }

    public String getPostal() {
        return postal;
    }

    public Building setPostal(String postal) {
        this.postal = postal;
        return this;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Building setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Building setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public ClimateZones getKoppen() {
        return koppen;
    }

    public Building setKoppen(ClimateZones koppen) {
        this.koppen = koppen;
        return this;
    }

    public String getNoaaStationId() {
        return noaaStationId;
    }

    public Building setNoaaStationId(String noaaStationId) {
        this.noaaStationId = noaaStationId;
        return this;
    }

    public String getEpwDataId() {
        return epwDataId;
    }

    public Building setEpwDataId(String epwDataId) {
        this.epwDataId = epwDataId;
        return this;
    }

    public Integer getYearBuilt() {
        return yearBuilt;
    }

    public Building setYearBuilt(Integer yearBuilt) {
        this.yearBuilt = yearBuilt;
        return this;
    }

    public PrimaryBuildingType getBuildingType() {
        return buildingType;
    }

    public Building setBuildingType(PrimaryBuildingType buildingType) {
        this.buildingType = buildingType;
        return this;
    }

    public Integer getGrossFloorAreaM2() {
        return grossFloorAreaM2;
    }

    public Building setGrossFloorAreaM2(Integer grossFloorAreaM2) {
        this.grossFloorAreaM2 = grossFloorAreaM2;
        return this;
    }

    public Integer getGrossFloorAreaFt2() {
        return grossFloorAreaFt2;
    }

    public Building setGrossFloorAreaFt2(Integer grossFloorAreaFt2) {
        this.grossFloorAreaFt2 = grossFloorAreaFt2;
        return this;
    }

    public Integer getGrossFloorArea() {
        return grossFloorArea;
    }

    public Building setGrossFloorArea(Integer grossFloorArea) {
        this.grossFloorArea = grossFloorArea;
        return this;
    }

    public GrossFloorAreaUnitsType getGrossFloorAreaUnit() {
        return grossFloorAreaUnit;
    }

    public Building setGrossFloorAreaUnit(GrossFloorAreaUnitsType grossFloorAreaUnit) {
        this.grossFloorAreaUnit = grossFloorAreaUnit;
        return this;
    }

    public Double getEnergyConsumptionIntensity() {
        return energyConsumptionIntensity;
    }

    public Building setEnergyConsumptionIntensity(Double energyConsumptionIntensity) {
        this.energyConsumptionIntensity = energyConsumptionIntensity;
        return this;
    }

    public Double getEnergyCostIntensity() {
        return energyCostIntensity;
    }

    public Building setEnergyCostIntensity(Double energyCostIntensity) {
        this.energyCostIntensity = energyCostIntensity;
        return this;
    }

    public Double getEmissionsIntensity() {
        return emissionsIntensity;
    }

    public Building setEmissionsIntensity(Double emissionsIntensity) {
        this.emissionsIntensity = emissionsIntensity;
        return this;
    }

    public Date getLastMeasurementDate() {
        return lastMeasurementDate;
    }

    public Building setLastMeasurementDate(Date lastMeasurementDate) {
        this.lastMeasurementDate = lastMeasurementDate;
        return this;
    }

    public String getNotes() {
        return notes;
    }

    public Building setNotes(String notes) {
        this.notes = notes;
        return this;
    }

    public Set<Use> getUses() {
        return uses;
    }

    public Building setUses(Set<Use> uses) {
        this.uses = uses;
        return this;
    }

    public Set<Identifier> getIdentifiers() {
        return identifiers;
    }

    public Building setIdentifiers(Set<Identifier> identifiers) {
        this.identifiers = identifiers;
        return this;
    }

    public LocationAddress getLocationDetails() {
        return locationDetails;
    }

    public Building setLocationDetails(LocationAddress locationDetails) {
        this.locationDetails = locationDetails;
        return this;
    }

    public enum GrossFloorAreaUnitsType {
        SQUARE_FEET,
        SQUARE_METERS
    }

    public enum PrimaryBuildingType {
        HOTEL,
        OFFICE,
        HOSPITAL,
        APARTMENT,
        DATA_CENTER,
        PRIMARY_SCHOOL,
        SECONDARY_SCHOOL,
        FULL_SERVICE_RESTAURANT,
        OUTPATIENT_HEALTH_CARE,
        QUICK_SERVICE_RESTAURANT,
        STAND_ALONE_RETAIL,
        SHOPPING_MALL,
        SUPERMARKET_GROCERY_STORE,
        REFRIGERATED_WAREHOUSE,
        NON_REFRIGERATED_WAREHOUSE
    }
}
